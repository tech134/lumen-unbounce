<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
// use Illuminate\Http\Request;

use Closure;

class FormMiddleware
{
    public function handle($request, Closure $next)
    {
                // grab some user
        $requestData = $request->all();
        $form_data = json_decode($requestData['data_json'], true);
        $campaign_id = $form_data['campaign_id'][0];
          // $token = $request->bearerToken();
        $token = $form_data['_token'][0];
        $campaign = \DB::table('campaign')->where('campaign_id',$campaign_id)->first();
        if (!$campaign) {
            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the 
            // below respose for now.
            return response()->json([
                'error' => 'Campaign does not exist.'
            ], 400);
        }

        foreach ($campaign as $data_campaign){
            $key = $campaign->campaign_key;
        }

        if(!$token) {
            // Unauthorized response if token not there
            return [
                'code' => 401,
                'error' => 'Token not provided.'
            ];
        }

        if (!($key == $token)) {
            return response()->json([
                'token' => "not valid"
            ], 401);
        }
                
        return $next($request);
    }
}
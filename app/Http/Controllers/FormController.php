<?php
 
namespace App\Http\Controllers;

use App\Models\Form_leads;

use App\Jobs\SendEmailForm;
use Carbon\Carbon;
use Illuminate\Support\Facades\Queue;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use Redirect,Response;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class FormController extends BaseController
{

  public function index(Request $request)
    {
    $data = Form_leads::orderBy('created_at', 'asc')->get();
    return response()->json($data, 201);
    }

        function show(Request $request, $lead_id)
    {
    // $id = Crypt::decrypt($campaign_id);
    $data = Form_leads::where('lead_id',$lead_id)->first();

            if(!$data) {
            // Unauthorized response if token not there
            return [
                'code' => 401,
                'error' => 'Leads not available.'
            ];
            }

    return response()->json($data, 201);
    }

        function destroy(Request $request, $lead_id)
    {
        $data = Form_leads::where('lead_id',$lead_id)->delete();

        $this->success = "Delete success";

        return response()->json($this->success, 201);
    }

    public function store(Request $request)
    {

          $requestData = $request->all();
          $form_data = json_decode($requestData['data_json'], true);

          $this->campaign_id = $form_data['campaign_id'][0];
          $phone_number = $form_data['phone_number'][0];
          $phone_number = preg_replace('/\s+/', '', $phone_number);
          $phone_number = str_replace("(","",$phone_number);
          $phone_number = str_replace(")","",$phone_number);
          $phone_number = str_replace(".","",$phone_number);
          $phone_number = str_replace("-","",$phone_number);
          $phone_number = str_replace("+","",$phone_number);
          
          if(substr(trim($phone_number), 0, 2)=='62'){
              $phone_number = substr_replace($phone_number,'0',0,2);
          }

          $name = $form_data['name'][0];
          $email = $form_data['email'][0];

          if (empty($form_data['attr_1'][0])){
              $attr_1 = "";
          }else{
              $attr_1 = $form_data['attr_1'][0];
          }
          if (empty($form_data['attr_2'][0])){
              $attr_2 = "";
          }else{
              $attr_2 = $form_data['attr_2'][0];
          }
          if (empty($form_data['attr_3'][0])){
              $attr_3 = "";
          }else{
              $attr_3 = $form_data['attr_3'][0];
          }
          if (empty($form_data['attr_4'][0])){
              $attr_4 = "";
          }else{
              $attr_4 = $form_data['attr_4'][0];
          }
          if (empty($form_data['attr_5'][0])){
              $attr_5 = "";
          }else{
              $attr_5 = $form_data['attr_5'][0];
          }

          if(isset($form_data['channel_id'][0])){
          $channel_id = $form_data['channel_id'][0];
          }else{
          $channel_id = "";
          }

          //$campaign_channel = $form_data->campaign_channel[0];

          $utm_medium = $form_data['utm_medium'][0];
          $utm_source = $form_data['utm_source'][0];
          $utm_campaign = $form_data['utm_campaign'][0];
          $utm_term = $form_data['utm_term'][0];
          $utm_content = $form_data['utm_content'][0];

          $page_id = $form_data['page_id'][0];
          $page_url = $form_data['page_url'][0];
          $variant = $form_data['variant'][0];
          $ip_address = $form_data['ip_address'][0];
          $page_name= $form_data['page_name'][0];
          $this->media = "LP-FORM";

          $phone1 = \DB::table('leads_call')->where('incomingNumber',$phone_number)->where('campaign_id',$this->campaign_id )->count();

          $phone2 = \DB::table('leads_chat')->where('phone_number',$phone_number)->where('campaign_id',$this->campaign_id )->count();

          $phone3 = \DB::table('leads_form')->where('phone_number',$phone_number)->where('campaign_id',$this->campaign_id )->count();
          
          $filter = \DB::table('leads_filter')->where('filter',$phone_number)->count();

          $junk = Form_leads::where('name',$name)->where('phone_number',$phone_number)->where('email',$email)->where('attr_1',$attr_1)->where('attr_2',$attr_2)->where('attr_3',$attr_3)->where('attr_4',$attr_4)->where('attr_5',$attr_5)->where('campaign_id', $this->campaign_id)->count();

          $phone = ($phone1 + $phone2 + $phone3);
          //junk leads will be close connections
          if ($junk > 0) {
          $junk_message = "You have a junk leads";
              return response()->json($junk_message, 201);
          }elseif ($filter > 0){
          $junk_message = "You have a spam leads";
              return response()->json($junk_message, 201);
          }else{
              
          if ($phone > 0) {
            $this->status = "Duplicate";
          }elseif (strpos($name, 'test') !== false) {
            $this->status = "Test";
          }elseif (strpos($email, 'test') !== false) {
            $this->status = "Test";
          }else{
            $this->status = "";
          }
              
          $datas = \DB::table('campaign')->where('campaign_id',$this->campaign_id)->first();
              
          $this->herovision = $datas->channel_id;
          $this->campaign_name =  $datas->campaign_name;
          $this->l_attr_1 =  $datas->attr_1;
          $this->l_attr_2 =  $datas->attr_2;
          $this->l_attr_3 =  $datas->attr_3;
          $this->l_attr_4 =  $datas->attr_4;
          $this->l_attr_5 =  $datas->attr_5;

          $to =  $datas->email_client;
          $cc1 =  $datas->cc1;
          $cc2 =  $datas->cc2;
          $cc3 =  $datas->cc3;
          $cc4 =  $datas->cc4;
          $this->crm_url =  $datas->crm_url;
          $this->crm =  $datas->crm;

          $this->accessKey =  $datas->accessKey;
          $this->secretKey =  $datas->secretKey;


          if ($utm_medium == ''){
                  $this->channel = "direct";
          }else{
                  $auto_placement = \DB::table('utm_medium')->where('utm_medium',$utm_medium)->get();
                  foreach ($auto_placement as $data){
                              $auto = $data->auto;
                          }
              
                  if (isset($auto)){
                      $this->channel = $auto;
                  }else{
                      $this->channel = "Undefined";
                  }         
          }
          
          if ($this->channel == 'direct' && $utm_source == 'Facebook'){
              $this->channel = "FB";
          }
                    
              
          $posts = \DB::table('leads_form')->where('lead_id', \DB::raw("(select max(lead_id) from leads_form)"))->get();
          if (count($posts)){
              foreach ($posts as $data){
              $d = (int)substr($data->lead_id,2,5);
              $d++;
              $char = "FM";
              $lead_id = $char . sprintf("%05s", $d);
          }
          }else{
              $lead_id = "FM00001";
          }

          // $this->time = Carbon::now('Asia/Jakarta'); 
        date_default_timezone_set('Asia/Jakarta');
        $this->time = date('Y-m-d H:i:s');    
          $data = new Form_leads();
          $data->lead_id = $lead_id;
          $data->name = $name;
          $data->email = $email;
          $data->phone_number = $phone_number;
          
          $data->attr_1 = $attr_1;
          $data->attr_2 = $attr_2;
          $data->attr_3 = $attr_3;
          $data->attr_4 = $attr_4;
          $data->attr_5 = $attr_5;
          $data->campaign_id = $this->campaign_id;
          $data->channel_id = $channel_id;

          $data->utm_medium = $utm_medium;
          $data->utm_source = $utm_source;
          $data->utm_campaign = $utm_campaign;
          $data->utm_term = $utm_term;
          $data->utm_content = $utm_content;
            
          $data->page_id = $page_id;
          $data->page_url = $page_url;
          $data->variant = $variant;
          $data->ip_address = $ip_address;
          $data->page_name = $page_name;
          $data->media = $this->media;
          $data->channel = $this->channel;
          $data->created_at = $this->time;
          $data->save();

          $jdata = [
                  "lead_id" => $lead_id,
                  "time" => $this->time,
                  "name" => $name,
                  "email" => $email,
                  "phone_number" => $phone_number,
                  "attr_1" => empty($attr_1) ? "" : $attr_1,
                  "attr_2" => empty($attr_2) ? "" : $attr_2,
                  "attr_3" => empty($attr_3) ? "" : $attr_3,
                  "attr_4" => empty($attr_4) ? "" : $attr_4,
                  "attr_5" => empty($attr_5) ? "" : $attr_5,
                  "channel" => $this->channel,
                  "media" => $this->media,
                  "utm_medium" => $utm_medium,
                  "utm_source" => $utm_source,
                  "utm_term" => $utm_term,
                  "utm_campaign" => $utm_campaign,
                  "utm_content" => $utm_content,
                  "ip_address" => $ip_address,
                  "page_url" => $page_url,
                  "page_name" => $page_name,
                  "channel_id" => $channel_id,
                  'status' => $this->status,
                  'campaign_name' => $this->campaign_name,
          ];


              //email notification
              $data_mail = [
                      'name' => $name,
                      'lead_id' => $lead_id,
                      'email' => $email,
                      'phone_number' => $phone_number,
                      'utm_medium' => $utm_medium,
                      'utm_content' => $utm_content,
                      'utm_source' => $utm_source,
                      'utm_term' => $utm_term,
                      'utm_campaign' => $utm_campaign,
                      'time' => $this->time,
                      'campaign_name' => $this->campaign_name,
                      'campaign_id' => $this->campaign_id,
                      'l_attr_1' => $this->l_attr_1,
                      'l_attr_2' => $this->l_attr_2,
                      'l_attr_3' => $this->l_attr_3,
                      'l_attr_4' => $this->l_attr_4,
                      'l_attr_5' => $this->l_attr_5,
                      'attr_1' => $attr_1,
                      'attr_2' => $attr_2,
                      'attr_3' => $attr_3,
                      'attr_4' => $attr_4,
                      'attr_5' => $attr_5,
                      'channel' => $this->channel,
                      'media' =>  $this->media,
                      'page_name' =>  $page_name,
                      'status' => $this->status,
                      'to' => $to,
                      'cc1' => $cc1,
                      'cc2' => $cc2,
                      'cc3' => $cc3,
                      'cc4' => $cc4,
              ];
      
//into heroagent
            if ($this->crm == "Google Sheets") {
              try {
                  $client = new Client(
                      [
                          'timeout'  => 5,
                          'headers' => [
                          "Content-Type" => 'application/json'
                          ]
                      ]
                  );
                  $json_response =  $client->request('POST', $this->crm_url,['json' => $jdata]);
                  Log::info(json_encode($json_response->getBody()));
              } catch (\Exception $ex) {
                  Log::critical($ex);
              }
            }

                        //into hero vision
              if ($this->herovision == "active") {
                try {
                    //for production url is https://leadservice-staging.heroleads.co.th/LandingPageCallService?token=thioch4eimovoiDu6ahd
                    $client = new Client(
                        [
                            'base_uri' => 'https://leadservice.heroleads.co.th/',
                            'timeout'  => 5,
                            'headers' => [
                                "Content-Type" => 'application/json'
                            ]
                        ]
                    );
                $json_response =  $client->request('POST','LandingPageCallService?token=thioch4eimovoiDu6ahd',['json' => $jdata]);
                Log::info(json_encode($json_response->getBody()));
            } catch (\Exception $ex) {
                Log::critical($ex);
            }
            }elseif($this->status == "Test"){
            }else{
              if(isset($to)){
                  Queue::later(Carbon::now()->addMinutes(1),new SendEmailForm($data_mail));
              }
            }

              $success = "Success";
              
              return response()->json($success, 201);
          }
      }
}
<?php

namespace App\Jobs;

use App\Mail\EmailForm as EmailForm;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
// use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendEmailForm implements ShouldQueue{

    // use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    /**
     * Create a new job instance.
     *
     * @param Carbon $currentDate
     */
    public function __construct($data)
    {
         $this->data = $data;
    }
    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle(){


        $email = new EmailForm($this->data);
        Mail::to($this->data['to'])->cc($this->data['cc1'])->cc($this->data['cc2'])->cc($this->data['cc3'])->cc($this->data['cc4'])->send($email);

    }
}